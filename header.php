<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); echo ' - '; bloginfo('description'); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/assets/icons/favicon.ico?v=2" rel="shortcut icon">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		
		<div class="homepage-hero-module">
			<div class="video-container">
				<div class="filter"></div>
				<video autoplay loop class="fillWidth">
					<source src="<?php echo get_template_directory_uri().'/assets/code/Hello-World.mp4'; ?>
" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
					<source src="<?php echo get_template_directory_uri().'/assets/code/Hello-World.webm'; ?>" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
				</video>
				<div class="poster hidden">
					<img src="<?php echo get_template_directory_uri().'/assets/code/Hello-World.jpg'; ?>" alt="">
				</div>
			</div>
		</div>

		<header class="header header--down" role="banner">
			<!-- Site Title -->
			<h1 class="header__logo ">
				<a class='header__logo--link' href="<?php echo home_url(); ?>"><?php echo bloginfo('name'); ?><br><span class="header__logo--link--description"> <?php bloginfo('description'); ?></span></a>
			</h1>

			<!-- Menu Toggle -->
			<div class='header__toggle ' id='menu-icon'>
				<span><i class="fa fa-bars fa-3x" aria-hidden="true"></i></span>
			</div>

			<!-- Menu -->
			<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'menu'            => 'primary',
						'menu_class'      => 'menu clear',
						'container'       => 'nav',
						'container_class' => 'header__nav',
						'echo'            => true
					)
				);
			?>
		</header>

			
