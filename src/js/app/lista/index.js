import React from "react";
import ReactDOM from "react-dom";


export default class NamnLista extends React.Component{

	render(){
		let domNyckel = 0;

		const list = this.props.namn.map( function(object){
			return <li key={domNyckel++}>{object}</li>
		})
		
		return(
			<ul>{list}</ul>
		);
	}
}