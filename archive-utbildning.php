<?php /* Template Name: utbildning */ ?>
<?php get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<main role="main" class="col-xs-12 col-md-9">
			<section>
				<h1>Utbildning</h1>
				<div class="row">
				<?php
                    $query = new WP_Query(['post_type' => 'utbildning']);
				    $posts = $query->posts;
					
					foreach ($posts as $post) { ?>
					    <div class="col-xs-12 col-md-6 col-lg-4">
                            <article class="article <?php echo $post->post_type; ?>" id="post-<?php echo $post->ID; ?>">
                                <div class="row">
                                    <!-- post thumbnail -->
                                    <?php if ( has_post_thumbnail() ) { ?>
                                        <div class="col-xs-12">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    
                                    <div class="col-xs-12">
                                        <!-- post -->
                                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                        <p><?php echo $post->post_content; ?></p>

                                        <!-- post details -->
                                        <p>
                                            <span class="postInfo">
                                                <?php 
                                                    $terms = get_the_terms($post->ID,'kompetens');
                                                    if ($terms) {
                                                        echo 'Kompetensområden: <br>';
                                                        foreach ($terms as $term) {
                                                            echo ' '.$term->name;
                                                        }
                                                    }
                                                ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </article>
					    </div>
				    <?php } ?>
				</div>
			</section>
		</main>
		<aside class="sidebar__aside sidebar-widget col-xs-12 col-md-3" role="complementary">
			<?php get_sidebar(); ?>
		</aside>
	</div>
</div>
<?php get_footer(); ?>
