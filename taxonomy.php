<?php get_header(); ?>
<div class="container-fluid">
	<div class="row">
		<main role="main" class="col-xs-12 col-md-9">
			<?php 
			$term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
			$args = array(
				'numberposts' => -1,
				'post_type'   => 'projekt',
                'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => 'kompetens',
                        'field'    => 'term_id',
                        'terms'    => $term->term_id,
                    ),
                ),
			);
            $query = new WP_Query( $args );
			$posts = $query->posts;
			if ($posts) { ?>
			<section>
				<h2>Projekt</h2>
				<div class="row">
				<?php foreach ($posts as $post) { ?>
					<div class="col-xs-12 col-md-6 col-lg-4">
						<article class="article <?php echo $post->post_type; ?>" id="post-<?php echo $post->ID; ?>">
							<div class="row">
								<!-- post thumbnail -->
								<?php if ( has_post_thumbnail() ) { ?>
									<div class="col-xs-12">
										<a href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail(); // Declare pixel size you need inside the array ?>
										</a>
									</div>
								<?php } ?>
								
								<div class="col-xs-12">
									<!-- post -->
									<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
									<p><?php echo $post->post_content; ?></p>

									<!-- post details -->
									<p>
										<span class="postInfo">
											<?php 
												$terms = get_the_terms($post->ID,'kompetens');
												if ($terms) {
													echo 'Kompetensområden: <br>';
													foreach ($terms as $term) {
														echo ' '.$term->name;
													}
												}
											?>
										</span>
									</p>
								</div>
							</div>
						</article>
					</div>
				<?php } ?>
				</div>
			</section>
			<?php } ?>
		</main>
		<aside class="sidebar__aside sidebar-widget col-xs-12 col-md-3" role="complementary">
			<?php get_sidebar(); ?>
		</aside>
	</div>
</div>
<?php get_footer(); ?>