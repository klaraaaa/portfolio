<?php
header("Access-Control-Allow-Origin: *");

error_reporting(E_ALL);
if ( ! is_admin() ) {
    require_once( ABSPATH . 'wp-admin/includes/post.php' );
}

ini_set('display_errors', 1);
define('__THEME__DIR__', get_template_directory_uri());
add_theme_support( 'post-thumbnails' );
$theme_pages = array(
  'Hem' => array('title' => 'Hem', 'template' => 'page-templates/page-landing.php')
);
    

function sidebars(){
    register_sidebar([
        'name' => "Aside widgets",
        'id' => "aside-sidebar"
    ]);
}
add_action('widgets_init','sidebars'); 

show_admin_bar( false );

function custom_enqueue_scripts() {
	wp_enqueue_style( 'Normalizer', 'https://cdnjs.cloudflare.com/ajax/libs/normalize/4.2.0/normalize.min.css' );
	wp_enqueue_style( 'font-awesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
	wp_enqueue_style( 'bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css' ); 
	wp_enqueue_style( 'theme-style', get_stylesheet_uri() );
	wp_enqueue_style( 'icons','https://fonts.googleapis.com/icon?family=Material+Icons' );
	wp_enqueue_script( 'myscript', get_template_directory_uri().'/public/scripts/customizer.js', array(), '1.0', true );
	wp_enqueue_script( 'jQuery CDN', 'http://code.jquery.com/jquery-3.1.1.min.js');
}

add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts' );

$theme_pages = array(
  'Utbildning' => array('title' => 'utbildning', 'template' => 'archive-utbildning.php'),
  'Projekt' => array('title' => 'projekt', 'template' => 'archive-projekt.php'),
  'Arbetsgivare' => array('title' => 'arbetsgivare', 'template' => 'archive-arbetsgivare.php')
);

$theme_competence_area = array(
    'CSS/SCSS',
    'PHP',
    'HTML',
    'Databashantering',
    'Wordpress',
    'Javascript/jQuery',
    'UX'
);

function create_competence_area() {
    register_taxonomy(
        'kompetens',
        'projekt',
        array(
            'label' => __( 'Kompetens' ),
            'rewrite' => array( 'slug' => 'kompetens' ),
            'hierarchical' => true,
        )
    );

    global $theme_competence_area;
    foreach ($theme_competence_area as $new_competence) {
        wp_insert_term(
        $new_competence,
        'kompetens',
        array(
            'description'	=> $new_competence,
            'slug' 		    => $new_competence
            )
        );
    }
}
add_action( 'init', 'create_competence_area' );
     
function register_my_menu() {
  register_nav_menu( 'primary', __( 'Primary' ) );
}
add_action( 'after_setup_theme', 'register_my_menu' );


/* HOOK things when activated the theme */
function activate_theme(){
    global $theme_pages;
    foreach ($theme_pages as $slug => $page){
        // Check that the page doesn't exist already
        $query = new WP_Query('pagename='.$slug);
        if(!$query->have_posts()){
            // Add the page using the data from the array above.
            wp_insert_post(
                array(
                    'post_name'      => $slug,
                    'post_title'     => $page['title'],
                    'post_status'    => 'publish',
                    'post_type'      => 'page',
                    'ping_status'    => 'closed',
                    'comment_status' => 'closed',
                    'meta_input'     => array('_wp_page_template' => $page['template'])
                )
            );
        }
    }   
}
add_action('after_switch_theme', 'activate_theme');

function register_custom_post_types(){
  $post_types = ["projekt", "utbildning", "arbetsgivare"];
    foreach($post_types as $type){
       if(!post_type_exists($type)){
           $labels = array(
            'name' => $type, 'post type general name',
            'singular_name' => $type, 'post type singular name',
            'add_new' => 'Add New', $type,
            'add_new_item' => 'Add New ',$type,
            'edit_item' => 'Edit',
            'new_item' => 'New',
            'all_items' => 'All',
            'view_item' => 'View',
            'search_items' => 'Search',
            'not_found' => 'None found',
            'not_found_in_trash' => 'None found in the Trash', 
            'parent_item_colon' => '',
            'menu_name' => ucfirst($type)
            );
            $args = array(
            'labels' => $labels,
            'singular_label' => ucfirst($type),
            'public' => true,
            'show_ui' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'rewrite' => true,
            'supports' => array('title', 'editor','excerpt','thumbnail'),
            'taxonomies' => array( 'kompetens' )
          );
          register_post_type($type, $args);
        }
    }
}
add_action( 'init', 'register_custom_post_types' );

function checkExist($params){ 
	$post = [];
	if(!post_exists($params["title"])){
		return registerObject($params);
	}else{
		global $wpdb;
		$postid = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '".$params["title"]."'" );
		$post = get_post($postid);
	}
	return $post;
}

function registerObject($params){
	if (isset ($params['parent'])) {
		$parent = $params['parent'];
	} else {
		$parent = 0;
	}
	$new_post = wp_insert_post( array(
		'post_author' => get_current_user_id(),
		'post_status' => 'publish',
		'post_content' => $params['content'],
		'post_title' => $params['title'],
		'post_type'=> $params['type'],
		'post_parent'=> $parent
		)
	);
	unset($params['title']);
	foreach ($params as $key => $value) {
		update_post_meta($new_post, $key, $value);
	}
	return get_post($new_post);
}
?>