<div class="sidebar__container">
	<?php if ( is_active_sidebar( 'aside-sidebar' ) ) : ?>
		<ul id="sidebar">
			<?php dynamic_sidebar( 'aside-sidebar' ); ?>
		</ul>
	<?php endif; ?>
</div>