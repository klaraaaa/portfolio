/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(3);


/***/ },
/* 1 */,
/* 2 */,
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	/* WEBPACK VAR INJECTION */(function(jQuery, $) {'use strict';jQuery(document).ready(function ($) {
	    //Tablet or smaller
	    if ($(window).width() <= 768) {
	        //toggla fram menyn
	        $('.header__toggle').on('click', function () {
	            $('.header__nav').toggle('slow');
	        });
	    }

	    scaleVideoContainer();

	    initBannerVideoSize('.video-container .poster img');
	    initBannerVideoSize('.video-container .filter');
	    initBannerVideoSize('.video-container video');

	    $(window).on('resize', function () {
	        scaleVideoContainer();
	        scaleBannerVideoSize('.video-container .poster img');
	        scaleBannerVideoSize('.video-container .filter');
	        scaleBannerVideoSize('.video-container video');
	    });

	});

	function scaleVideoContainer() {

	    var height = $(window).height() + 5;
	    var unitHeight = parseInt(height) + 'px';
	    $('.homepage-hero-module').css('height', unitHeight);

	}

	function initBannerVideoSize(element) {

	    $(element).each(function () {
	        $(this).data('height', $(this).height());
	        $(this).data('width', $(this).width());
	    });

	    scaleBannerVideoSize(element);

	}

	function scaleBannerVideoSize(element) {

	    var windowWidth = $(window).width(),
	    windowHeight = $(window).height() + 5,
	    videoWidth,
	    videoHeight;

	    console.log(windowHeight);

	    $(element).each(function () {
	        var videoAspectRatio = $(this).data('height') / $(this).data('width');

	        $(this).width(windowWidth);

	        if (windowWidth < 1000) {
	            videoHeight = windowHeight;
	            videoWidth = videoHeight / videoAspectRatio;
	            $(this).css({ 'margin-top': 0, 'margin-left': -(videoWidth - windowWidth) / 2 + 'px' });

	            $(this).width(videoWidth).height(videoHeight);
	        }

	        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

	    });
	}



	// Hide Header on on scroll down
	var didScroll;
	var lastScrollTop = 0;
	var delta = 5;
	var navbarHeight = $('header').outerHeight();

	$(window).scroll(function (event) {
	    didScroll = true;
	});

	setInterval(function () {
	    if (didScroll) {
	        hasScrolled();
	        didScroll = false;
	    }
	}, 250);

	function hasScrolled() {
	    var st = $(window).scrollTop();

	    // Make sure they scroll more than delta
	    if (Math.abs(lastScrollTop - st) <= delta)
	    return;

	    // If they scrolled down and are past the navbar, add class .header__nav--up.
	    // This is necessary so you never see what is "behind" the navbar.
	    if (st > lastScrollTop && st > navbarHeight) {
	        // Scroll Down
	        $('.header').removeClass('header--down').addClass('header--up');
	    } else {
	        // Scroll Up
	        if (st + $(window).height() < $(document).height()) {
	            $('.header').removeClass('header--up').addClass('header--down');
	        }
	    }

	    lastScrollTop = st;
	}
	/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(4), __webpack_require__(4)))

/***/ },
/* 4 */
/***/ function(module, exports) {

	module.exports = jQuery;

/***/ }
/******/ ]);