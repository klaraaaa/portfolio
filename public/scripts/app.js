/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports) {

	
	// import React from "react";
	// import ReactDOM from "react-dom";
	// import Lista from "./lista";

	// export default class CoolaKidsPaKontoret extends React.Component{

	// 	constructor(props){
	// 	 super(props);

	// 	 this.state = {
	// 	 	namn: ["Klara", "Robin", "Ante"],
	// 	 	heading: "Coola Kids på DigitalHub"
	// 	 }

	// 	 this.laggTillNamn = this.laggTillNamn.bind(this);
	// 	}


	// 	laggTillNamn(event){
	// 		if(event.key == "Enter"){
	// 		 const nyaNamn = this.state.namn.concat(event.target.value);
	// 		 this.setState({namn: nyaNamn});
	// 		}
	// 	}

	// 	render(){
	// 		return(
	// 			<div>
	// 				<h1>{this.state.heading}</h1>
	// 				<input onKeyPress={this.laggTillNamn} ref="namn" />
	// 				<Lista namn={this.state.namn} />
	// 			</div>
	// 		)
	// 	}
	// }


	// ReactDOM.render(<CoolaKidsPaKontoret />, document.getElementById("app"));
	"use strict";

/***/ },
/* 2 */
/***/ function(module, exports) {

	// removed by extract-text-webpack-plugin

/***/ }
/******/ ]);